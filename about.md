---
title: About
layout: article
---

This is my personal blog. It's built with Jekyll and hosted on Gitlab. The
[full source is available here](https://gitlab.com/jetpks/eric.jacobs.af).

A [more comprehensive bio is available here](http://jacobs.af/eric.html).
