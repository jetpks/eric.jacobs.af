# References

* [https://tianqi.name/jekyll-TeXt-theme/docs/en/quick-start](https://tianqi.name/jekyll-TeXt-theme/docs/en/quick-start)
* [https://github.com/jekyll/jemoji](https://github.com/jekyll/jemoji)
* [https://gitlab.com/jetpks/jacobs.af](https://gitlab.com/jetpks/jacobs.af)
* [https://rubygems.org/search?utf8=%E2%9C%93&query=jekyll](https://rubygems.org/search?utf8=%E2%9C%93&query=jekyll)
